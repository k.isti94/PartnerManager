-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 15, 2015 at 07:48 AM
-- Server version: 5.5.43-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `symfony`
--

-- --------------------------------------------------------

--
-- Table structure for table `location`
--

CREATE TABLE IF NOT EXISTS `location` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `partner` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `country` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `postalcode` int(11) NOT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `publicspace` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` int(11) NOT NULL,
  `telephone` int(11) DEFAULT NULL,
  `fax` int(11) DEFAULT NULL,
  `mobile` int(11) DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `defaultlocation` tinyint(1) NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `userid` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `location`
--

INSERT INTO `location` (`id`, `partner`, `name`, `country`, `postalcode`, `state`, `publicspace`, `address`, `telephone`, `fax`, `mobile`, `email`, `defaultlocation`, `description`, `userid`) VALUES
(1, 5, 'asd', '1', 1, 'asd', 'asd', 1, 1, 1, 1, 'asd@asd.hu', 0, NULL, 1),
(2, 5, 'asd', '0', 1, 'asd', 'asd', 1, 1, 1, 1, 'asd@asd.hu', 1, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `partner`
--

CREATE TABLE IF NOT EXISTS `partner` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` int(11) NOT NULL,
  `companyname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `prefix` int(11) DEFAULT NULL,
  `lastname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `firstname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `billingname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `partnertype` int(11) NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `headquarterscountry` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `headquarterspostalcode` int(11) NOT NULL,
  `headquartersstation` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `headquarterspublicspace` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `headquartersaddress` int(11) NOT NULL,
  `billingcountry` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `billingpostalcode` int(11) NOT NULL,
  `billingstation` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `billingpublicspace` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `billingaddress` int(11) NOT NULL,
  `mailingcountry` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mailingpostalcode` int(11) NOT NULL,
  `mailingstation` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mailingpublicspace` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mailingaddress` int(11) NOT NULL,
  `taxnumber` int(11) DEFAULT NULL,
  `eutaxnumber` int(11) DEFAULT NULL,
  `companyregistrationnumber` int(11) DEFAULT NULL,
  `telephone` int(11) DEFAULT NULL,
  `fax` int(11) DEFAULT NULL,
  `mobile` int(11) DEFAULT NULL,
  `birthday` datetime DEFAULT NULL,
  `mothername` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `IDcard` int(11) DEFAULT NULL,
  `userId` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

-- --------------------------------------------------------

--
-- Table structure for table `prefix`
--

CREATE TABLE IF NOT EXISTS `prefix` (
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `userid` int(11) NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=15 ;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username_canonical` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email_canonical` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `salt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `locked` tinyint(1) NOT NULL,
  `expired` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  `confirmation_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password_requested_at` datetime DEFAULT NULL,
  `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  `credentials_expired` tinyint(1) NOT NULL,
  `credentials_expire_at` datetime DEFAULT NULL,
  `IDNevElotag` int(11) DEFAULT NULL,
  `NevElotag` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Keresztnev` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Vezeteknev` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_8D93D64992FC23A8` (`username_canonical`),
  UNIQUE KEY `UNIQ_8D93D649A0D96FBF` (`email_canonical`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `username_canonical`, `email`, `email_canonical`, `enabled`, `salt`, `password`, `last_login`, `locked`, `expired`, `expires_at`, `confirmation_token`, `password_requested_at`, `roles`, `credentials_expired`, `credentials_expire_at`, `IDNevElotag`, `NevElotag`, `Keresztnev`, `Vezeteknev`) VALUES
(3, 'teszt', 'teszt', 'teszt@teszt.hu', 'teszt@teszt.hu', 1, 'th9f3g6h034ok4wgsgkckggc4w48gwg', '$2y$13$th9f3g6h034ok4wgsgkckeMbtKFvnFnUcsWUok.Z7hUrwBtO102/O', '2015-10-15 07:47:31', 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, NULL, NULL, 'teszt', 'teszt');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
