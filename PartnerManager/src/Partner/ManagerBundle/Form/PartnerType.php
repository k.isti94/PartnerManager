<?php

namespace Partner\ManagerBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Partner\ManagerBundle\Form\PrefixType;

class PartnerType extends AbstractType
{
    protected $selectedentities;
    protected $countries;
    
    public function __construct($selectedEntities = null) {
        $this->selectedentities = $selectedEntities;
        $this->countries = array("Afghanistan", "Albania", "Algeria", "American Samoa", "Andorra", "Angola", "Anguilla", "Antarctica", "Antigua and Barbuda", "Argentina", "Armenia", "Aruba", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia and Herzegowina", "Botswana", "Bouvet Island", "Brazil", "British Indian Ocean Territory", "Brunei Darussalam", "Bulgaria", "Burkina Faso", "Burundi", "Cambodia", "Cameroon", "Canada", "Cape Verde", "Cayman Islands", "Central African Republic", "Chad", "Chile", "China", "Christmas Island", "Cocos (Keeling) Islands", "Colombia", "Comoros", "Congo", "Congo, the Democratic Republic of the", "Cook Islands", "Costa Rica", "Cote d'Ivoire", "Croatia (Hrvatska)", "Cuba", "Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "East Timor", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Falkland Islands (Malvinas)", "Faroe Islands", "Fiji", "Finland", "France", "France Metropolitan", "French Guiana", "French Polynesia", "French Southern Territories", "Gabon", "Gambia", "Georgia", "Germany", "Ghana", "Gibraltar", "Greece", "Greenland", "Grenada", "Guadeloupe", "Guam", "Guatemala", "Guinea", "Guinea-Bissau", "Guyana", "Haiti", "Heard and Mc Donald Islands", "Holy See (Vatican City State)", "Honduras", "Hong Kong", "Hungary", "Iceland", "India", "Indonesia", "Iran (Islamic Republic of)", "Iraq", "Ireland", "Israel", "Italy", "Jamaica", "Japan", "Jordan", "Kazakhstan", "Kenya", "Kiribati", "Korea, Democratic People's Republic of", "Korea, Republic of", "Kuwait", "Kyrgyzstan", "Lao, People's Democratic Republic", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libyan Arab Jamahiriya", "Liechtenstein", "Lithuania", "Luxembourg", "Macau", "Macedonia, The Former Yugoslav Republic of", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Marshall Islands", "Martinique", "Mauritania", "Mauritius", "Mayotte", "Mexico", "Micronesia, Federated States of", "Moldova, Republic of", "Monaco", "Mongolia", "Montserrat", "Morocco", "Mozambique", "Myanmar", "Namibia", "Nauru", "Nepal", "Netherlands", "Netherlands Antilles", "New Caledonia", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Niue", "Norfolk Island", "Northern Mariana Islands", "Norway", "Oman", "Pakistan", "Palau", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Pitcairn", "Poland", "Portugal", "Puerto Rico", "Qatar", "Reunion", "Romania", "Russian Federation", "Rwanda", "Saint Kitts and Nevis", "Saint Lucia", "Saint Vincent and the Grenadines", "Samoa", "San Marino", "Sao Tome and Principe", "Saudi Arabia", "Senegal", "Seychelles", "Sierra Leone", "Singapore", "Slovakia (Slovak Republic)", "Slovenia", "Solomon Islands", "Somalia", "South Africa", "South Georgia and the South Sandwich Islands", "Spain", "Sri Lanka", "St. Helena", "St. Pierre and Miquelon", "Sudan", "Suriname", "Svalbard and Jan Mayen Islands", "Swaziland", "Sweden", "Switzerland", "Syrian Arab Republic", "Taiwan, Province of China", "Tajikistan", "Tanzania, United Republic of", "Thailand", "Togo", "Tokelau", "Tonga", "Trinidad and Tobago", "Tunisia", "Turkey", "Turkmenistan", "Turks and Caicos Islands", "Tuvalu", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "United States", "United States Minor Outlying Islands", "Uruguay", "Uzbekistan", "Vanuatu", "Venezuela", "Vietnam", "Virgin Islands (British)", "Virgin Islands (U.S.)", "Wallis and Futuna Islands", "Western Sahara", "Yemen", "Yugoslavia", "Zambia", "Zimbabwe");
    }
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        
        $builder
                
            ->add('type','choice', array(
                    'choices' => array(0 => 'Cég', 1 => 'Maganszemely'),
                    'required' => true,
                    ))
            ->add('name')
            ->add('companyname')
            ->add('prefix','entity' ,array(
                    'empty_value' => null,
                    'class' => 'ManagerBundle:Prefix',
                    'choices' => $this->selectedentities,
                    'property' => 'name',
                    'data' => null,
            ))
            ->add('lastname')
            ->add('firstname')
            ->add('billingname','text',array('label'=> 'Billing name'))
            ->add('partnertype','choice',array(
                'label'=> 'Partner type',
                'choices' => array(
                    0 => 'Customer', 1 => 'Supplier', 2 => 'Customer and Supplier', 3 => 'Prospective partner' 
                )))
            ->add('email')
            ->add('headquarterscountry','choice',array(
                'label'=> 'Headquarters country',
                'choices' => $this->countries
                ))
            ->add('headquarterspostalcode',null,array('label'=> 'Headquarters postalcode'))
            ->add('headquartersstation','text',array('label'=> 'Headquarters station'))
            ->add('headquarterspublicspace','text',array('label'=> 'Headquarters public space'))
            ->add('headquartersaddress',null,array('label'=> 'Headquarters address'))
            ->add('billingcountry','choice',array('label'=> 'Billing country',
                'choices' => $this->countries))
            ->add('billingpostalcode',null,array('label'=> 'Billing postalcode'))
            ->add('billingstation','text',array('label'=> 'Billing station'))
            ->add('billingpublicspace','text',array('label'=> 'Billing public space'))
            ->add('billingaddress',null,array('label'=> 'Billing address'))
            ->add('mailingcountry','choice',array('label'=> 'Mailing country',
                'choices' => $this->countries))
            ->add('mailingpostalcode',null,array('label'=> 'Mailing postalcode'))
            ->add('mailingstation','text',array('label'=> 'Mailing station'))
            ->add('mailingpublicspace','text',array('label'=> 'Mailing public space'))
            ->add('mailingaddress',null,array('label'=> 'Mailing address'))
            ->add('taxnumber')
            ->add('eutaxnumber',null,array('label'=> 'EU taxnumber'))
            ->add('companyregistrationnumber','text',array('label'=> 'Company registration number'))
            ->add('telephone')
            ->add('fax')
            ->add('mobile')
            ->add('birthday')
            ->add('mothername')
            ->add('iDcard',null,array('label'=>'ID card'))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Partner\ManagerBundle\Entity\Partner'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'partner_managerbundle_partner';
    }
}
