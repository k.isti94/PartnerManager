<?php

namespace Partner\ManagerBundle\Validator\Constraints;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class PrefixNameOnlyOneValidator extends ConstraintValidator
{

    private $entityManager;
    private $security_context;
    
    public function __construct(EntityManager $entityManager,$security_context)
    {
        $this->entityManager = $entityManager;
        $this->security_context = $security_context;
    }
    
    public function validate($value, Constraint $constraint)
    {
        if ($this->usedThisName($value)) {
            // If you're using the new 2.5 validation API (you probably are!)
            $this->context->buildViolation($constraint->message)
                ->setParameter('%string%', $value)
                ->addViolation();

            // If you're using the old 2.4 validation API
            /*
            $this->context->addViolation(
                $constraint->message,
                array('%string%' => $value)
            );
            */
        }
    }
    private function usedThisName($name){
        $em = $this->entityManager->getRepository('ManagerBundle:Prefix')->findBy(array(
            'userid' => $this->security_context->getToken()->getUser()->getId()
                ));
        
        $vane=false;
        foreach ($em as $c) {
            if ($name == $c->getName()){
                $vane= true;
            }
        }
        return $vane;
    }
}
