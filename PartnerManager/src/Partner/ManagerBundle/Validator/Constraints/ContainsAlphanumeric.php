<?php

namespace Partner\ManagerBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class ContainsAlphanumeric extends Constraint
{
    public $message = 'The password:"%string%" is not good. The password MUST contain at least 2 of these: an upper/lower character and a number';
}
