<?php

namespace Partner\ManagerBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class ContainsAlphanumericValidator extends ConstraintValidator
{

    public function validate($value, Constraint $constraint)
    {
        if ($this->goodPass($value)) {
            // If you're using the new 2.5 validation API (you probably are!)
            $this->context->buildViolation($constraint->message)
                ->setParameter('%string%', $value)
                ->addViolation();

            // If you're using the old 2.4 validation API
            /*
            $this->context->addViolation(
                $constraint->message,
                array('%string%' => $value)
            );
            */
        }
    }
    private function goodPass($pass){
        $upper = array('Q','W','E','R','T','Z','U','I','O','P','A','S','D','F','G','H','J','K','L','Y','X','C','V','B','N','M');
        $upperbool=false;
        $lower = array('q','w','e','r','t','z','u','i','o','p','a','s','d','f','g','h','j','k','l','y','x','c','v','b','n','m');
        $lowerbool = false;
        $number = array('0','1','2','3','4','5','6','7','8','9');
        $numberbool = false;
        foreach ($upper as  $c){
            if(strpos($pass, $c)!== false){
                    $upperbool=true;
            }
        }
        foreach ($lower as  $c){
            if(strpos($pass, $c) !== false){
                    $lowerbool=true;
            }
        }
        foreach ($number as  $c){
            if(strpos($pass, $c)!== false){
                    $numberbool=true;
            }
            $last=$c;
        }
        if (($upperbool == true && $lowerbool == true) ||
                ($numberbool == true && $lowerbool == true) ||
                ($numberbool == true && $upperbool == true)){
            return false;
        }
        return true;
    }
}
