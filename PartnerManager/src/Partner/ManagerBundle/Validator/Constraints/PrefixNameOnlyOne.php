<?php

namespace Partner\ManagerBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class PrefixNameOnlyOne extends Constraint
{
    public $message = 'This name:"%string%" has already use';
    
    public function validatedBy()
    {
        return 'prefix.name.only.one.validator';
    }
}
