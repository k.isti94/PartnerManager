<?php

namespace Partner\ManagerBundle\Entity;


use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Partner\ManagerBundle\Validator\Constraints as ManagerAssert;

/**
 * User
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Partner\ManagerBundle\Entity\UserRepository")
 */
class User extends BaseUser
{
    public function __construct()
    {
        parent::__construct();
    }
    
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     * @Assert\NotBlank
     * @ManagerAssert\ContainsAlphanumeric
     * )
     */
    protected $plainPassword;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="IDNevElotag", type="integer", nullable=true)
     */
    protected $iDNevElotag;

    /**
     * @var string
     *
     * @ORM\Column(name="NevElotag", type="string", length=255, nullable=true)
     */
    private $nevElotag;

    /**
     * @var string
     *
     * @ORM\Column(name="Keresztnev", type="string", length=255)
     */
    private $keresztnev;

    /**
     * @var string
     *
     * @ORM\Column(name="Vezeteknev", type="string", length=255)
     */
    private $vezeteknev;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set iDNevElotag
     *
     * @param integer $iDNevElotag
     *
     * @return User
     */
    public function setIDNevElotag($iDNevElotag)
    {
        $this->iDNevElotag = $iDNevElotag;

        return $this;
    }

    /**
     * Get iDNevElotag
     *
     * @return integer
     */
    public function getIDNevElotag()
    {
        return $this->iDNevElotag;
    }

    /**
     * Set nevElotag
     *
     * @param string $nevElotag
     *
     * @return User
     */
    public function setNevElotag($nevElotag)
    {
        $this->nevElotag = $nevElotag;

        return $this;
    }

    /**
     * Get nevElotag
     *
     * @return string
     */
    public function getNevElotag()
    {
        return $this->nevElotag;
    }

    /**
     * Set keresztnev
     *
     * @param string $keresztnev
     *
     * @return User
     */
    public function setKeresztnev($keresztnev)
    {
        $this->keresztnev = $keresztnev;

        return $this;
    }

    /**
     * Get keresztnev
     *
     * @return string
     */
    public function getKeresztnev()
    {
        return $this->keresztnev;
    }

    /**
     * Set vezeteknev
     *
     * @param string $vezeteknev
     *
     * @return User
     */
    public function setVezeteknev($vezeteknev)
    {
        $this->vezeteknev = $vezeteknev;

        return $this;
    }

    /**
     * Get vezeteknev
     *
     * @return string
     */
    public function getVezeteknev()
    {
        return $this->vezeteknev;
    }
}

