<?php

namespace Partner\ManagerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Partner
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Partner\ManagerBundle\Entity\PartnerRepository")
 */
class Partner
{
    /**
     * @var integer
     * @ORM\OneToMany(targetEntity="Location", mappedBy="partner")
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="type", type="integer")
     */
    private $type;
    
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="companyname", type="string", length=255, nullable=true)
     */
    private $companyname;

    /**
     * @var integer
     * 
     * @ORM\ManyToOne(targetEntity="Prefix", inversedBy="id")
     * @ORM\Column(name="prefix", type="integer", nullable=true)
     */
    private $prefix;

    /**
     * @var string
     *
     * @ORM\Column(name="lastname", type="string", length=255, nullable=true)
     */
    private $lastname;

    /**
     * @var string
     *
     * @ORM\Column(name="firstname", type="string", length=255, nullable=true)
     */
    private $firstname;

    /**
     * @var string
     *
     * @ORM\Column(name="billingname", type="string", length=255, nullable=true)
     */
    private $billingname;

    /**
     * @var integer
     *
     * @ORM\Column(name="partnertype", type="integer")
     */
    private $partnertype;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=true)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="headquarterscountry", type="string", length=255)
     */
    private $headquarterscountry;

    /**
     * @var integer
     *
     * @ORM\Column(name="headquarterspostalcode", type="integer")
     */
    private $headquarterspostalcode;

    /**
     * @var string
     *
     * @ORM\Column(name="headquartersstation", type="string", length=255)
     */
    private $headquartersstation;

    /**
     * @var string
     *
     * @ORM\Column(name="headquarterspublicspace", type="string", length=255)
     */
    private $headquarterspublicspace;

    /**
     * @var integer
     *
     * @ORM\Column(name="headquartersaddress", type="integer")
     */
    private $headquartersaddress;

    /**
     * @var string
     *
     * @ORM\Column(name="billingcountry", type="string", length=255)
     */
    private $billingcountry;

    /**
     * @var integer
     *
     * @ORM\Column(name="billingpostalcode", type="integer")
     */
    private $billingpostalcode;

    /**
     * @var string
     *
     * @ORM\Column(name="billingstation", type="string", length=255)
     */
    private $billingstation;

    /**
     * @var string
     *
     * @ORM\Column(name="billingpublicspace", type="string", length=255)
     */
    private $billingpublicspace;

    /**
     * @var integer
     *
     * @ORM\Column(name="billingaddress", type="integer")
     */
    private $billingaddress;

    /**
     * @var string
     *
     * @ORM\Column(name="mailingcountry", type="string", length=255)
     */
    private $mailingcountry;

    /**
     * @var integer
     *
     * @ORM\Column(name="mailingpostalcode", type="integer")
     */
    private $mailingpostalcode;

    /**
     * @var string
     *
     * @ORM\Column(name="mailingstation", type="string", length=255)
     */
    private $mailingstation;

    /**
     * @var string
     *
     * @ORM\Column(name="mailingpublicspace", type="string", length=255)
     */
    private $mailingpublicspace;

    /**
     * @var integer
     *
     * @ORM\Column(name="mailingaddress", type="integer")
     */
    private $mailingaddress;

    /**
     * @var integer
     *
     * @ORM\Column(name="taxnumber", type="integer", nullable=true)
     */
    private $taxnumber;

    /**
     * @var integer
     *
     * @ORM\Column(name="eutaxnumber", type="integer", nullable=true)
     */
    private $eutaxnumber;

    /**
     * @var integer
     *
     * @ORM\Column(name="companyregistrationnumber", type="integer", nullable=true)
     */
    private $companyregistrationnumber;

    /**
     * @var integer
     *
     * @ORM\Column(name="telephone", type="integer", nullable=true)
     */
    private $telephone;

    /**
     * @var integer
     *
     * @ORM\Column(name="fax", type="integer", nullable=true)
     */
    private $fax;

    /**
     * @var integer
     *
     * @ORM\Column(name="mobile", type="integer", nullable=true)
     */
    private $mobile;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="birthday", type="datetime", nullable=true)
     */
    private $birthday;

    /**
     * @var string
     *
     * @ORM\Column(name="mothername", type="string", length=255, nullable=true)
     */
    private $mothername;

    /**
     * @var integer
     *
     * @ORM\Column(name="iDcard", type="integer", nullable=true)
     */
    private $iDcard;

    /**
     * @var integer
     *
     * @ORM\Column(name="userId", type="integer")
     */
    private $userId;

    /**
     * Set userid
     *
     * @param integer $userId
     *
     * @return Prefix
     */
    public function setUserid($userId)
    {
        $this->userId = $userId;

        return $this;
    }
    
    /**
     * Get userid
     *
     * @return integer
     */
    public function getUserid()
    {
        return $this->userId;
    }
    
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param integer $type
     *
     * @return Partner
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return integer
     */
    public function getType()
    {
        return $this->type;
    }
    
    /**
     * Set companyname
     *
     * @param string $name
     *
     * @return Partner
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set companyname
     *
     * @param string $companyname
     *
     * @return Partner
     */
    public function setCompanyname($companyname)
    {
        $this->companyname = $companyname;

        return $this;
    }

    /**
     * Get companyname
     *
     * @return string
     */
    public function getCompanyname()
    {
        return $this->companyname;
    }

    /**
     * Set prefix
     *
     * @param integer $prefix
     *
     * @return Partner
     */
    public function setPrefix($prefix)
    {
        $this->prefix = $prefix;

        return $this;
    }

    /**
     * Get prefix
     *
     * @return integer
     */
    public function getPrefix()
    {
        return $this->prefix;
    }

    /**
     * Set lastname
     *
     * @param string $lastname
     *
     * @return Partner
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * Get lastname
     *
     * @return string
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * Set firstname
     *
     * @param string $firstname
     *
     * @return Partner
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * Get firstname
     *
     * @return string
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Set billingname
     *
     * @param string $billingname
     *
     * @return Partner
     */
    public function setBillingname($billingname)
    {
        $this->billingname = $billingname;

        return $this;
    }

    /**
     * Get billingname
     *
     * @return string
     */
    public function getBillingname()
    {
        return $this->billingname;
    }

    /**
     * Set partnertype
     *
     * @param integer $partnertype
     *
     * @return Partner
     */
    public function setPartnertype($partnertype)
    {
        $this->partnertype = $partnertype;

        return $this;
    }

    /**
     * Get partnertype
     *
     * @return integer
     */
    public function getPartnertype()
    {
        return $this->partnertype;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Partner
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set headquarterscountry
     *
     * @param string $headquarterscountry
     *
     * @return Partner
     */
    public function setHeadquarterscountry($headquarterscountry)
    {
        $this->headquarterscountry = $headquarterscountry;

        return $this;
    }

    /**
     * Get headquarterscountry
     *
     * @return string
     */
    public function getHeadquarterscountry()
    {
        return $this->headquarterscountry;
    }

    /**
     * Set headquarterspostalcode
     *
     * @param integer $headquarterspostalcode
     *
     * @return Partner
     */
    public function setHeadquarterspostalcode($headquarterspostalcode)
    {
        $this->headquarterspostalcode = $headquarterspostalcode;

        return $this;
    }

    /**
     * Get headquarterspostalcode
     *
     * @return integer
     */
    public function getHeadquarterspostalcode()
    {
        return $this->headquarterspostalcode;
    }

    /**
     * Set headquartersstation
     *
     * @param string $headquartersstation
     *
     * @return Partner
     */
    public function setHeadquartersstation($headquartersstation)
    {
        $this->headquartersstation = $headquartersstation;

        return $this;
    }

    /**
     * Get headquartersstation
     *
     * @return string
     */
    public function getHeadquartersstation()
    {
        return $this->headquartersstation;
    }

    /**
     * Set headquarterspublicspace
     *
     * @param string $headquarterspublicspace
     *
     * @return Partner
     */
    public function setHeadquarterspublicspace($headquarterspublicspace)
    {
        $this->headquarterspublicspace = $headquarterspublicspace;

        return $this;
    }

    /**
     * Get headquarterspublicspace
     *
     * @return string
     */
    public function getHeadquarterspublicspace()
    {
        return $this->headquarterspublicspace;
    }

    /**
     * Set headquartersaddress
     *
     * @param integer $headquartersaddress
     *
     * @return Partner
     */
    public function setHeadquartersaddress($headquartersaddress)
    {
        $this->headquartersaddress = $headquartersaddress;

        return $this;
    }

    /**
     * Get headquartersaddress
     *
     * @return integer
     */
    public function getHeadquartersaddress()
    {
        return $this->headquartersaddress;
    }

    /**
     * Set billingcountry
     *
     * @param string $billingcountry
     *
     * @return Partner
     */
    public function setBillingcountry($billingcountry)
    {
        $this->billingcountry = $billingcountry;

        return $this;
    }

    /**
     * Get billingcountry
     *
     * @return string
     */
    public function getBillingcountry()
    {
        return $this->billingcountry;
    }

    /**
     * Set billingpostalcode
     *
     * @param integer $billingpostalcode
     *
     * @return Partner
     */
    public function setBillingpostalcode($billingpostalcode)
    {
        $this->billingpostalcode = $billingpostalcode;

        return $this;
    }

    /**
     * Get billingpostalcode
     *
     * @return integer
     */
    public function getBillingpostalcode()
    {
        return $this->billingpostalcode;
    }

    /**
     * Set billingstation
     *
     * @param string $billingstation
     *
     * @return Partner
     */
    public function setBillingstation($billingstation)
    {
        $this->billingstation = $billingstation;

        return $this;
    }

    /**
     * Get billingstation
     *
     * @return string
     */
    public function getBillingstation()
    {
        return $this->billingstation;
    }

    /**
     * Set billingpublicspace
     *
     * @param string $billingpublicspace
     *
     * @return Partner
     */
    public function setBillingpublicspace($billingpublicspace)
    {
        $this->billingpublicspace = $billingpublicspace;

        return $this;
    }

    /**
     * Get billingpublicspace
     *
     * @return string
     */
    public function getBillingpublicspace()
    {
        return $this->billingpublicspace;
    }

    /**
     * Set billingaddress
     *
     * @param integer $billingaddress
     *
     * @return Partner
     */
    public function setBillingaddress($billingaddress)
    {
        $this->billingaddress = $billingaddress;

        return $this;
    }

    /**
     * Get billingaddress
     *
     * @return integer
     */
    public function getBillingaddress()
    {
        return $this->billingaddress;
    }

    /**
     * Set mailingcountry
     *
     * @param string $mailingcountry
     *
     * @return Partner
     */
    public function setMailingcountry($mailingcountry)
    {
        $this->mailingcountry = $mailingcountry;

        return $this;
    }

    /**
     * Get mailingcountry
     *
     * @return string
     */
    public function getMailingcountry()
    {
        return $this->mailingcountry;
    }

    /**
     * Set mailingpostalcode
     *
     * @param integer $mailingpostalcode
     *
     * @return Partner
     */
    public function setMailingpostalcode($mailingpostalcode)
    {
        $this->mailingpostalcode = $mailingpostalcode;

        return $this;
    }

    /**
     * Get mailingpostalcode
     *
     * @return integer
     */
    public function getMailingpostalcode()
    {
        return $this->mailingpostalcode;
    }

    /**
     * Set mailingstation
     *
     * @param string $mailingstation
     *
     * @return Partner
     */
    public function setMailingstation($mailingstation)
    {
        $this->mailingstation = $mailingstation;

        return $this;
    }

    /**
     * Get mailingstation
     *
     * @return string
     */
    public function getMailingstation()
    {
        return $this->mailingstation;
    }

    /**
     * Set mailingpublicspace
     *
     * @param string $mailingpublicspace
     *
     * @return Partner
     */
    public function setMailingpublicspace($mailingpublicspace)
    {
        $this->mailingpublicspace = $mailingpublicspace;

        return $this;
    }

    /**
     * Get mailingpublicspace
     *
     * @return string
     */
    public function getMailingpublicspace()
    {
        return $this->mailingpublicspace;
    }

    /**
     * Set mailingaddress
     *
     * @param integer $mailingaddress
     *
     * @return Partner
     */
    public function setMailingaddress($mailingaddress)
    {
        $this->mailingaddress = $mailingaddress;

        return $this;
    }

    /**
     * Get mailingaddress
     *
     * @return integer
     */
    public function getMailingaddress()
    {
        return $this->mailingaddress;
    }

    /**
     * Set taxnumber
     *
     * @param integer $taxnumber
     *
     * @return Partner
     */
    public function setTaxnumber($taxnumber)
    {
        $this->taxnumber = $taxnumber;

        return $this;
    }

    /**
     * Get taxnumber
     *
     * @return integer
     */
    public function getTaxnumber()
    {
        return $this->taxnumber;
    }

    /**
     * Set eutaxnumber
     *
     * @param integer $eutaxnumber
     *
     * @return Partner
     */
    public function setEutaxnumber($eutaxnumber)
    {
        $this->eutaxnumber = $eutaxnumber;

        return $this;
    }

    /**
     * Get eutaxnumber
     *
     * @return integer
     */
    public function getEutaxnumber()
    {
        return $this->eutaxnumber;
    }

    /**
     * Set companyregistrationnumber
     *
     * @param integer $companyregistrationnumber
     *
     * @return Partner
     */
    public function setCompanyregistrationnumber($companyregistrationnumber)
    {
        $this->companyregistrationnumber = $companyregistrationnumber;

        return $this;
    }

    /**
     * Get companyregistrationnumber
     *
     * @return integer
     */
    public function getCompanyregistrationnumber()
    {
        return $this->companyregistrationnumber;
    }

    /**
     * Set telephone
     *
     * @param integer $telephone
     *
     * @return Partner
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;

        return $this;
    }

    /**
     * Get telephone
     *
     * @return integer
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * Set fax
     *
     * @param integer $fax
     *
     * @return Partner
     */
    public function setFax($fax)
    {
        $this->fax = $fax;

        return $this;
    }

    /**
     * Get fax
     *
     * @return integer
     */
    public function getFax()
    {
        return $this->fax;
    }

    /**
     * Set mobile
     *
     * @param integer $mobile
     *
     * @return Partner
     */
    public function setMobile($mobile)
    {
        $this->mobile = $mobile;

        return $this;
    }

    /**
     * Get mobile
     *
     * @return integer
     */
    public function getMobile()
    {
        return $this->mobile;
    }

    /**
     * Set birthday
     *
     * @param \DateTime $birthday
     *
     * @return Partner
     */
    public function setBirthday($birthday)
    {
        $this->birthday = $birthday;

        return $this;
    }

    /**
     * Get birthday
     *
     * @return \DateTime
     */
    public function getBirthday()
    {
        return $this->birthday;
    }

    /**
     * Set mothername
     *
     * @param string $mothername
     *
     * @return Partner
     */
    public function setMothername($mothername)
    {
        $this->mothername = $mothername;

        return $this;
    }

    /**
     * Get mothername
     *
     * @return string
     */
    public function getMothername()
    {
        return $this->mothername;
    }

    /**
     * Set iDcard
     *
     * @param integer $iDcard
     *
     * @return Partner
     */
    public function setIDcard($iDcard)
    {
        $this->iDcard = $iDcard;

        return $this;
    }

    /**
     * Get iDcard
     *
     * @return integer
     */
    public function getIDcard()
    {
        return $this->iDcard;
    }
    
    public function __toString()
   {
      return strval( $this->getId() );
   }
}

