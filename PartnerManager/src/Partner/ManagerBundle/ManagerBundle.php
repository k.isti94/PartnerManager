<?php

namespace Partner\ManagerBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class ManagerBundle extends Bundle
{
    public function getParent() {
        return 'FOSUserBundle';
    }
}
