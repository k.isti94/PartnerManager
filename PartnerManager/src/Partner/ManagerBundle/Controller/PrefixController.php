<?php

namespace Partner\ManagerBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Partner\ManagerBundle\Entity\Prefix;
use Partner\ManagerBundle\Form\PrefixType;
use Partner\ManagerBundle\Form\CreatePrefixFormType;

/**
 * Prefix controller.
 *
 */
class PrefixController extends Controller
{

    /**
     * Lists all Prefix entities.
     *
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('ManagerBundle:Prefix')->findBy(array("userid"=>$this->getUser()->getId()));
        $pagination = $this->setpagination($request, $this->getUser()->getId());

        return $this->render('ManagerBundle:Prefix:index.html.twig', array(
            'entities' => $entities,
            'pagination' => $pagination,
        ));
    }
    public function setpagination(Request $request,$id )
    {
        $em = $this->getDoctrine()->getManager();
        
        $entities = $em->getRepository('ManagerBundle:Prefix')->findBy(array("userid" => $id));

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $entities,

            $request->query->getInt('page', 1)/*page number*/,
            $request->query->getInt('limit', 5)/*limit per page*/

        );
        $pagination->setCustomParameters(array(
            'class' => "pagination",
        ));

        // parameters to template
        return $pagination;
    }
    /**
     * Creates a new Prefix entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Prefix();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity->setUserid($this->getUser()->getId());
            $em->persist($entity);
            
            $em->flush();

            return $this->redirect($this->generateUrl('prefix_show', array('id' => $entity->getId())));
        }

        return $this->render('ManagerBundle:Prefix:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Prefix entity.
     *
     * @param Prefix $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Prefix $entity)
    {
        $form = $this->createForm(new CreatePrefixFormType(), $entity, array(
            'action' => $this->generateUrl('prefix_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Prefix entity.
     *
     */
    public function returnForm(){
        $entity = new Prefix();
        $form   = $this->createCreateForm($entity);
        return $form;
    }
    
    public function newAction()
    {
        $entity = new Prefix();
        $form   = $this->createCreateForm($entity);

        return $this->render('ManagerBundle:Prefix:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Prefix entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ManagerBundle:Prefix')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Prefix entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('ManagerBundle:Prefix:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Prefix entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ManagerBundle:Prefix')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Prefix entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('ManagerBundle:Prefix:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Prefix entity.
    *
    * @param Prefix $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Prefix $entity)
    {
        $form = $this->createForm(new PrefixType(), $entity, array(
            'action' => $this->generateUrl('prefix_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Prefix entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ManagerBundle:Prefix')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Prefix entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('prefix_edit', array('id' => $id)));
        }

        return $this->render('ManagerBundle:Prefix:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Prefix entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('ManagerBundle:Prefix')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Prefix entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('prefix'));
    }

    /**
     * Creates a form to delete a Prefix entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('prefix_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
