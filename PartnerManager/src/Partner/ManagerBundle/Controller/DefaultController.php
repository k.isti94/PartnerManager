<?php

namespace Partner\ManagerBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        if ($this->isGranted("IS_AUTHENTICATED_REMEMBERED")) {
            return $this->redirect('/partner');
        }
        return $this->redirect('/login');
    }
}
