Tekepítés menete:

Terminalba a következő parancsokat kell megadni:

    - git clone git@gitlab.com:k.isti94/PartnerManager.git
    
    - cd PartnerManager
    
    - curl -sS https://getcomposer.org/installer | php

    - php composer.phar install

    - php composer.phar update
    
Ezután el kell inditani a szervert:
    
    - php app/console server:run
    
Csatolt sql fájlt beimportálva kapunk egy felhasználót, aminek a neve: teszt jelszava: tesztTeszt

Lehet regisztrálni is új felhasználót.
Belépés után elérhetőek a felületek: 

    - Prefix: Itt új név előtagot leeht felvenni, és megnézni az eddigieket, valamint szerkeszteni azokat
    
    - Partner: Új partnert lehet felvenni, megnézni és szerkezteni azokat
    
    - Location: Új telephelyet lehet felvenni, valamint megnézni és szerkeszteni azokat.